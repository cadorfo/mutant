package com.cadorfo.mutant.repositories;

import com.cadorfo.mutant.model.Dna;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DnaRepository extends MongoRepository<Dna, Integer> {

    Optional<Dna> findFirstByHashCode(int hashCode);
}
