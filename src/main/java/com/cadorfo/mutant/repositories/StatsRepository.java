package com.cadorfo.mutant.repositories;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatsRepository {

    private RedisTemplate<String, Long> redisTemplate;

    public StatsRepository(RedisTemplate<String, Long> redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    public void increment(String key) {
        redisTemplate.opsForValue().increment(key);
    }

    public List<Long> multiGet(List<String> keys) {
        return redisTemplate.opsForValue().multiGet(keys);
    }

    public void set(String key, Long value) {
        redisTemplate.opsForValue().set(key, value);
    }
}
