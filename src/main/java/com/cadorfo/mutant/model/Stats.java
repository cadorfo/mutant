package com.cadorfo.mutant.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Stats {
    @JsonProperty("count_mutant_dna")
    private long mutant;
    @JsonProperty("count_human_dna")
    private long normal;
    private double ratio;
}
