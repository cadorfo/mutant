package com.cadorfo.mutant.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class Dna {
    @Id
    private String id;

    private Boolean mutant;

    private String[] sequence ;

    @Indexed(unique = true)
    private int hashCode;
}
