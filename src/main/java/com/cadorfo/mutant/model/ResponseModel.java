package com.cadorfo.mutant.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseModel {
    private Boolean mutant;
}
