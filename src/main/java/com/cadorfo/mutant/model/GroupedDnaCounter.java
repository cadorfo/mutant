package com.cadorfo.mutant.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupedDnaCounter {

    @Id
    private Boolean mutant;
    private Long total;
}
