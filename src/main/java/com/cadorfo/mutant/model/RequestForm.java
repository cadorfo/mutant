package com.cadorfo.mutant.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Data
public class RequestForm {
    @NotNull
    private ArrayList<String> dna;
}
