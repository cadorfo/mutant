package com.cadorfo.mutant.controllers;

import com.cadorfo.mutant.model.RequestForm;
import com.cadorfo.mutant.model.ResponseModel;
import com.cadorfo.mutant.services.MutantProcessService;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Log
@RestController
public class DNAController {

    private MutantProcessService mutantProcessService;

    public DNAController(MutantProcessService mutantProcessService) {
        this.mutantProcessService = mutantProcessService;
    }

    @PostMapping("/mutant/")
    public ResponseEntity processDNA(@Valid @RequestBody RequestForm dna) {

        ResponseModel mutant = mutantProcessService.processMutantDNA(dna.getDna());
        if(mutant.getMutant()){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
