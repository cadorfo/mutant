package com.cadorfo.mutant.controllers;

import com.cadorfo.mutant.model.Stats;
import com.cadorfo.mutant.services.StatsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatsController {


    private StatsService statsService;

    public StatsController(StatsService statsService) {
        this.statsService = statsService;
    }


    @GetMapping("/stats")
    public Stats getStats() {
        return statsService.getStat();
    }
}
