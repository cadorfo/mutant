package com.cadorfo.mutant.services;

import com.cadorfo.mutant.model.GroupedDnaCounter;
import com.cadorfo.mutant.model.Stats;
import com.cadorfo.mutant.repositories.StatsRepository;
import lombok.extern.java.Log;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

import static com.cadorfo.mutant.Constants.HUMAN;
import static com.cadorfo.mutant.Constants.MUTANT;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Service
@Log
public class StatsService {

    private MongoTemplate mongoTemplate;
    private StatsRepository statsRepository;

    public StatsService(MongoTemplate mongoTemplate, StatsRepository statsRepository) {
        this.mongoTemplate = mongoTemplate;
        this.statsRepository = statsRepository;
    }

    @PostConstruct
    public void init() {
        extractStatsFromMongo();
    }

    public Stats getStat() {
        List<Long> longs = statsRepository.multiGet(Arrays.asList(MUTANT, HUMAN));

        if (!longs.contains(null) && longs.size() > 1) {
            Long mutantCount = longs.get(0);
            Long normalCount = longs.get(1);
            Long totalCount = normalCount + mutantCount;
            return Stats.builder()
                    .mutant(mutantCount)
                    .normal(normalCount)
                    .ratio(totalCount == 0 ? 0 : mutantCount.doubleValue() / totalCount)
                    .build();
        }

        return extractStatsFromMongo();
    }

    private Stats extractStatsFromMongo() {

        List<GroupedDnaCounter> mappedResults = mongoTemplate.aggregate(
                newAggregation(
                        project("mutant"),
                        group("mutant").count().as("total")
                ),
                "dna",
                GroupedDnaCounter.class
        ).getMappedResults();

        Long mutantCount = 0L;
        Long normalCount = 0L;

        for (GroupedDnaCounter item : mappedResults) {
            if (item.getMutant()) {
                mutantCount = item.getTotal();
            } else {
                normalCount = item.getTotal();
            }
        }

        statsRepository.set(HUMAN, normalCount);
        statsRepository.set(MUTANT, mutantCount);

        Long total = normalCount + mutantCount;
        return Stats.builder()
                .mutant(mutantCount)
                .normal(normalCount)
                .ratio(total == 0 ? 0 : mutantCount.doubleValue() / total)
                .build();
    }
}
