package com.cadorfo.mutant.services;

import com.cadorfo.mutant.model.Dna;
import com.cadorfo.mutant.model.ResponseModel;
import com.cadorfo.mutant.repositories.DnaRepository;
import com.cadorfo.mutant.repositories.StatsRepository;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.cadorfo.mutant.Constants.HUMAN;
import static com.cadorfo.mutant.Constants.MUTANT;

@Service
@Log
public class MutantProcessService {

    private DnaRepository dnaRepository;

    private StatsRepository statsRepository;

    public MutantProcessService(DnaRepository dnaRepository, StatsRepository statsRepository) {
        this.dnaRepository = dnaRepository;
        this.statsRepository = statsRepository;
    }

    public ResponseModel processMutantDNA(List<String> dnaSequence) {

        Optional<Dna> oneByHashCode = dnaRepository.findFirstByHashCode(dnaSequence.hashCode());

        if (!oneByHashCode.isPresent()) {
            String[] dnaArray = dnaSequence.toArray(new String[dnaSequence.size()]);

            Dna dna = new Dna();
            dna.setId(UUID.randomUUID().toString());
            dna.setMutant(isMutant(dnaArray));
            dna.setSequence(dnaArray);
            dna.setHashCode(dnaSequence.hashCode());
            dnaRepository.insert(dna);
            updateRedis(dna.getMutant());
            return ResponseModel.builder().mutant(dna.getMutant()).build();
        } else {
            return ResponseModel.builder().mutant(oneByHashCode.get().getMutant()).build();
        }
    }

    private void updateRedis(Boolean isMutant) {
        if (isMutant) {
            statsRepository.increment(MUTANT);
        } else {
            statsRepository.increment(HUMAN);
        }
    }

    public boolean isMutant(String[] dna) {
        int linesCount = dna.length;
        int columnCount = dna[0].length();

        if (findInLine(dna, linesCount, columnCount)) return true;

        if (findInColumn(dna, linesCount, columnCount)) return true;

        return findInAxis(dna, linesCount, columnCount);


    }

    private boolean findInAxis(String[] dna, int linesCount, int columnCount) {
        for (int i = 0; i < linesCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                int nextX = i + 1;
                int nextY = j + 1;
                if (linesCount != nextX &&
                        columnCount != nextY &&
                        dna[i].charAt(j)
                                == dna[nextX].charAt(nextY)) {
                    return findNext(dna, nextX, nextY) >= 2;
                }
            }
        }
        return false;
    }

    private int findNext(String[] dna, int indexX, int indexY) {
        int nextX = 1 + indexX;
        int nextY = 1 + indexY;
        if (dna.length == nextY ||
                dna[indexX].length() == nextX) {
            return 0;
        }

        if (dna[indexX].charAt(indexY) == dna[nextX].charAt(nextY)) {
            return 1 + findNext(dna, nextX, nextY);
        }

        return 0;
    }

    private boolean findInColumn(String[] dna, int linesCount, int columnCount) {
        for (int i = 0; i < columnCount; i++) {
            int found = 0;
            for (int j = 0; j < linesCount - 1; j++) {
                int next = j + 1;
                if (dna[j].charAt(i) == dna[next].charAt(i)) {
                    found++;
                } else {
                    found = 0;
                }

                if (found == 3) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean findInLine(String[] dna, int linesCount, int columnCount) {
        for (int i = 0; i < linesCount; i++) {
            int found = 0;
            for (int j = 0; j < columnCount - 1; j++) {
                int next = j + 1;
                if (dna[i].charAt(j) == dna[i].charAt(next)) {
                    found++;
                } else {
                    found = 0;
                }

                if (found == 3) {
                    return true;
                }
            }
        }
        return false;
    }
}
