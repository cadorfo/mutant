package com.cadorfo.mutant.controllers;

import com.cadorfo.mutant.model.RequestForm;
import com.cadorfo.mutant.model.ResponseModel;
import com.cadorfo.mutant.services.MutantProcessService;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DNAControllerTest {

    @Mock
    private MutantProcessService mutantProcessService;

    @Test
    public void processDNAHuman() {

        String[] dnaSequence = {
                "ABBBF",
                "AAABA",
                "BCAFC",
                "AABAA",
                "AAFAB"
        };
        when(mutantProcessService.processMutantDNA(any(ArrayList.class)))
                .thenReturn(ResponseModel.builder().mutant(false).build());

        DNAController dnaController = dnaController();

        RequestForm requestForm = new RequestForm();
        requestForm.setDna(new ArrayList(Lists.list(dnaSequence)));
        ResponseEntity responseEntity = dnaController.processDNA(requestForm);

        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.FORBIDDEN);
    }

    @Test
    public void processDNAMutant() {

        String[] dnaSequence = {
                "ABBBF",
                "AAABA",
                "BCAFC",
                "AABAA",
                "AAFAB"
        };
        when(mutantProcessService.processMutantDNA(any(ArrayList.class)))
                .thenReturn(ResponseModel.builder().mutant(true).build());

        DNAController dnaController = dnaController();

        RequestForm requestForm = new RequestForm();
        requestForm.setDna(new ArrayList(Lists.list(dnaSequence)));
        ResponseEntity responseEntity = dnaController.processDNA(requestForm);

        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }


    private DNAController dnaController(){
        return new DNAController(mutantProcessService);
    }
}