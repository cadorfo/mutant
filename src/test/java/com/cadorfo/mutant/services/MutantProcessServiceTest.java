package com.cadorfo.mutant.services;

import com.cadorfo.mutant.model.Dna;
import com.cadorfo.mutant.model.ResponseModel;
import com.cadorfo.mutant.repositories.DnaRepository;
import com.cadorfo.mutant.repositories.StatsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MutantProcessServiceTest {

    @Mock
    DnaRepository dnaRepository;

    @Mock
    StatsRepository statsRepository;

    @Test
    public void list(){
        MutantProcessService mutantProcessService = createMutantProcessService();
        String[] dna = {
                "ABBBF",
                "AAAAA",
                "CCCCC"
        };
        boolean mutant = mutantProcessService.isMutant(dna);
        Assert.assertTrue(mutant);
    }

    @Test
    public void noMatch(){
        MutantProcessService mutantProcessService = createMutantProcessService();
        String[] dna = {
                "ABBBF",
                "AAGAA",
                "CCFCC"
        };
        boolean mutant = mutantProcessService.isMutant(dna);
        Assert.assertFalse(mutant);
    }

    @Test
    public void findColumn(){
        MutantProcessService mutantProcessService = createMutantProcessService();
        String[] dna = {
                "ABBBF",
                "AFABA",
                "ACCCC",
                "AABAA",
                "AAFAB"
        };
        boolean mutant = mutantProcessService.isMutant(dna);
        Assert.assertTrue(mutant);
    }

    @Test
    public void findAxis(){
        MutantProcessService mutantProcessService = createMutantProcessService();
        String[] dna = {
                "ABBBF",
                "AAABA",
                "BCAFC",
                "AABAA",
                "AAFAB"
        };
        boolean mutant = mutantProcessService.isMutant(dna);
        Assert.assertTrue(mutant);
    }


    @Test
    public void alreadHasBeenProcessed(){
        MutantProcessService mutantProcessService = createMutantProcessService();
        String[] dnaSequence = {
                "ABBBF",
                "AAABA",
                "BCAFC",
                "AABAA",
                "AAFAB"
        };
        Dna dna = new Dna();

        dna.setMutant(true);
        dna.setSequence(dnaSequence);


        when(dnaRepository.findFirstByHashCode(any(Integer.class))).thenReturn(Optional.of(dna));

        ResponseModel responseModel = mutantProcessService
                .processMutantDNA(Arrays.asList(dnaSequence));
        Assert.assertTrue(responseModel.getMutant());
    }

    @Test
    public void alreadHasnotBeenProcessed(){
        MutantProcessService mutantProcessService = createMutantProcessService();
        String[] dnaSequence = {
                "ABBBF",
                "AAABA",
                "BCAFC",
                "AABAA",
                "AAFAB"
        };
        when(dnaRepository.findFirstByHashCode(any(Integer.class))).thenReturn(Optional.empty());

        ResponseModel responseModel = mutantProcessService
                .processMutantDNA(Arrays.asList(dnaSequence));
        Assert.assertTrue(responseModel.getMutant());
    }


    private MutantProcessService createMutantProcessService(){
        return new MutantProcessService(dnaRepository,statsRepository);
    }
}
