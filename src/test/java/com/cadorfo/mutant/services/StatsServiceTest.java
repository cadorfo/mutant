package com.cadorfo.mutant.services;

import com.cadorfo.mutant.model.GroupedDnaCounter;
import com.cadorfo.mutant.model.Stats;
import com.cadorfo.mutant.repositories.StatsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;

import java.util.Arrays;
import java.util.List;

import static com.cadorfo.mutant.Constants.HUMAN;
import static com.cadorfo.mutant.Constants.MUTANT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatsServiceTest {

    @Mock
    private MongoTemplate mongoTemplate;

    @Mock
    private StatsRepository statsRepository;

    @Test
    public void getStatOnCache() {
        when(statsRepository.multiGet(eq(Arrays.asList(MUTANT, HUMAN))))
                .thenReturn(Arrays.asList(1L, 2L));


        StatsService statsService = createStatsService();
        Stats stat = statsService.getStat();

        Assert.assertEquals(1L, stat.getMutant());
        Assert.assertEquals(2L, stat.getNormal());
    }


    @Test
    public void getStatOnMongo() {
        List<GroupedDnaCounter> mappedResults = Arrays.asList(
                new GroupedDnaCounter(false, 4L),
                new GroupedDnaCounter(true, 3L)
        );
        AggregationResults<GroupedDnaCounter> aggregationResults = mock(AggregationResults.class);
        when(statsRepository.multiGet(eq(Arrays.asList(MUTANT, HUMAN))))
                .thenReturn(Arrays.asList(0L));
        when(mongoTemplate.aggregate(any(Aggregation.class), eq("dna"), eq(GroupedDnaCounter.class)))
                .thenReturn(aggregationResults);
        when(aggregationResults.getMappedResults()).thenReturn(mappedResults);

        StatsService statsService = createStatsService();
        Stats stat = statsService.getStat();

        Assert.assertEquals(3L, stat.getMutant());
        Assert.assertEquals(4L, stat.getNormal());
    }

    private StatsService createStatsService() {
        return new StatsService(mongoTemplate, statsRepository);
    }
}