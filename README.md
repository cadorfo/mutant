# Mutant Service

Apesar do problema ser proposto em espanhol como não falo essa lingua farei a explicação em português

Tecnologias:

- Java 8
- Spring Boot 2


Requisitos pra rodar sem Docker:

 - Redis
 - MongoDB 
 
 ```
 ./mvnw clean install
 ./mvnw spring-boot:run
 ```
 
 Para rodar com docker
 
 - Docker 
 - Docker compose
 
 ```
 ./buildAndRun.sh
 ```
 
 Comentários
 
 - O algoritmo utilizado para encontrar mutantes foi _naive_ nada otimizado e não poderia ser utilizado em um caso de molécula real de DNA (que possui 3,2 bilhões de bases nitrogenadas). 
 Em um caso real método de processamento dessa molécula não seria por uma API "quente" e precisaria de um método de processamento assíncrono (mesmo que imitando um endpoint síncrono) 
 com um cluster Spark, Flink, etc. Mas é claro que tudo dependeria principalmente do orçamento do Magneto.
 
 - Para tratar a singularidade das moléculas, utilizei um calculo de hashcode simples. Em um cenário real isso não seria possível, uma vez que os limites do tipo `Integer` geraria muitas colisões se considerarmos todos os habitantes do planeta. 
Assim, seria necessário utilizar outro algoritmo hashing, que consome mais recursos computacionais. Porém, o conceito é o mesmo. 

 - Para o caso de consulta de stats, utilizei o Redis como principal banco de consulta. A ideia é explorar suas qualidades de alto throughput e incremento atômico,
  evitando problemas de concorrência e inconsistência na atualização de estatisticas.
 
 - Para guardar os dados brutos, utilizei o MongoDB. A intenção é, além de ter um fallback, utilizar um banco rápido, com capacidades de clusterização e operações comuns de map-reduce. 
 Assim é possível fazer agregações das estatísticas, recalcular e explorar novas features ou ideias.
 
 # Endpoints
 
 Porta padrão 8080
 
 Verifica mutante
 
 
 ```
 POST /mutant/
 Payload: 
 {
    "dna": ["AAAAA",
            "AGTCA",
            "CCTCC",
            "TTGTT",
            "GGCGG"]
 }
 
 ```
 
 Estatística
 ```
 GET /stats
 ```